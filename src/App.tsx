import {
  createTheme,
  CssBaseline,
  StyledEngineProvider,
  ThemeProvider,
} from "@mui/material";
import createPalette from "@mui/material/styles/createPalette";
import "./App.css";
import Router from "./molecules/Router/Router";

const theme = createTheme({
  palette: createPalette({
    primary: {
      main: "#00685b",
      light: "#439688",
      dark: "#003d32",
      contrastText: "#ffffff",
    },
    secondary: {
      main: "#12b5a2",
      light: "#5fe8d3",
      dark: "#008473",
      contrastText: "#ffffff",
    },
  }),
});

const App = () => {
  return ( 
    <StyledEngineProvider injectFirst>
      <ThemeProvider theme={theme}>
        <CssBaseline />
        <Router />
      </ThemeProvider>
    </StyledEngineProvider>
  );
};

export default App;
