import { ConfigType } from "./config.types";

const CONTENT_BASE_URL = "kroon-hosting.nl";

const environment: ConfigType = {
    CONTENT_IMAGE_URL: `https://uploads.${CONTENT_BASE_URL}`,
    CONTENT_BASE_URL: `https://content.${CONTENT_BASE_URL}/graphql`
}

export default environment