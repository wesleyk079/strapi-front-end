import testEnvironment from "./testEnvironment";
import prodEnvironment from "./prodEnvironment";
import { ConfigType } from "./config.types";

const config: ConfigType =
  window.location.host === "localhost:3000" ? testEnvironment : prodEnvironment;

export default config;
