export type ConfigType = {
  CONTENT_IMAGE_URL: string;
  CONTENT_BASE_URL: string;
};
