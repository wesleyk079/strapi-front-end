import { ConfigType } from "./config.types";

const CONTENT_BASE_URL = "http://localhost:1337";

const testEnvironment: ConfigType = {
  CONTENT_IMAGE_URL: `${CONTENT_BASE_URL}`,
  CONTENT_BASE_URL: `${CONTENT_BASE_URL}/graphql`,
};

export default testEnvironment;
