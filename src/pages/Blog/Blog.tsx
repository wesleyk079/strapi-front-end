import { Box, Container, Grid, List, ListItem, Typography } from "@mui/material";
import { FC, useCallback, useEffect, useState } from "react";
import MediaCard from "../../molecules/MediaCard/MediaCard";
import ListCard from "../../molecules/ListCard/ListCard";
import ImageCard from "../../molecules/ImageCard/ImageCard";
import queries from "../../queries/queries";
import {
  Blogposts,
  Category,
  ComponentContentBlokRatingListInput,
  ComponentPageContentBlog,
  GetBlogPostsQuery,
} from "../../types";
import ReactMarkdown from "react-markdown";
import { Star } from "@mui/icons-material";
import { yellow } from "@mui/material/colors";
import { Link, Route, Switch } from "react-router-dom";

type BlogPost = {
  titel: string;
  technieken: ComponentContentBlokRatingListInput[];
};

const getStars = (amount: number) => {
  let icons: JSX.Element[] = [];
  for (let i = 0; i < amount; i++) {
    icons.push(<Star key={`star-${i}`} sx={{ color: yellow["800"] }} />);
  }
  return icons;
};

type NewType = {
  category: ComponentPageContentBlog["Category"];
};

const Blog: FC<NewType> = ({ category }) => {
  const [posts, setPosts] = useState<Blogposts[]>([]);

  useEffect(() => {
    const fireQuery = async () => {
      if (category) {
        const content = await queries.getBlogPostsByCategory({
          id: category.id,
        });
        if (content.blogposts) {
          setPosts(content.blogposts as Blogposts[]);
        }
      }
    };
    fireQuery();
  }, []);

  return (
    <List>
      {posts?.map(
        (item) => (<ListItem>
          <Link to={location => ({...location, pathname: `${location.pathname}/${item.id}`})}>{item.Titel}</Link>
          </ListItem>        )
      )}
    </List>
  );
};

export default Blog;
