import { Typography } from "@mui/material";
import { FC } from "react";
import ReactMarkdown from "react-markdown";
import { ComponentPageContentPaginaContent } from "../../types";

const HomePage: FC<Partial<ComponentPageContentPaginaContent>> = ({
  RichText,
  Titel,
}) => {
  return (
    <>
      <Typography variant="h1">{Titel}</Typography>
      {RichText && <ReactMarkdown>{RichText}</ReactMarkdown>}
    </>
  );
};

export default HomePage;
