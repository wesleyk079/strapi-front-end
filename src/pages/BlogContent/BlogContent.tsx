import { FC, useEffect, useState } from "react";
import { Box, Container, Grid, Typography } from "@mui/material";
import { Blogposts } from "../../types";
import { yellow } from "@mui/material/colors";
import { Star } from "@mui/icons-material";
import ImageCard from "../../molecules/ImageCard/ImageCard";
import ListCard from "../../molecules/ListCard/ListCard";
import MediaCard from "../../molecules/MediaCard/MediaCard";
import { ReactMarkdown } from "react-markdown/lib/react-markdown";
import { useParams } from "react-router-dom";
import queries from "../../queries/queries";

const getStars = (amount: number) => {
  let icons: JSX.Element[] = [];
  for (let i = 0; i < amount; i++) {
    icons.push(<Star key={`star-${i}`} sx={{ color: yellow["800"] }} />);
  }
  return icons;
};

const BlogContent: FC = () => {
  const { id } = useParams<{ id: string }>();

  const [item, setItem] = useState<Blogposts>();

  useEffect(() => {
    const fireQuery = async () => {
      if (id) {
        const content = await queries.getBlogPostsContent({
          id,
        });
        if (content) {
          setItem(content.blogpost as Blogposts);
        }
      }
    };
    fireQuery();
  }, []);

  return (
    <Container>
      <Grid container spacing={2} display="flex" alignItems="stretch">
        <Grid item xs={12} md={8} display="flex" direction="column">
          <Typography variant="h3" sx={{ marginBottom: 2 }}>
            {item?.Titel}
          </Typography>
          <ImageCard />
        </Grid>
        <Grid item xs>
          <ListCard
            items={item?.Technieken || undefined}
            render={(item) => {
              return (
                <Grid container justifyContent="space-between">
                  <Grid item>{item?.Titel}</Grid>
                  <Grid item> {getStars(item?.complexity_rating || 0)}</Grid>
                </Grid>
              );
            }}
          />
        </Grid>
      </Grid>
      <Box sx={{ marginTop: 3 }}>
        {item?.Content ? (
          item.Content.map((content) => {
            if (content?.__typename === "ComponentContentBlokMedia") {
              return (
                <MediaCard
                  source={content.image?.url}
                  alt={content.image?.alternativeText || "Onbeschikbaar"}
                />
              );
            }

            if (content?.__typename === "ComponentContentBlokRichText") {
              return (
                <ReactMarkdown>
                  {content.Text || "Geen content bechikbaar"}
                </ReactMarkdown>
              );
            }

            return <Typography>Geen content beschikbaar</Typography>;
          })
        ) : (
          <Typography>Geen content beschikbaar</Typography>
        )}
      </Box>
    </Container>
  );
};

export default BlogContent;
