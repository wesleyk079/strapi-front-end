import { loader } from "graphql.macro";
import config from "../config/config";
import {
  GetBlogPostsContentQuery,
  GetBlogPostsContentQueryVariables,
  GetBlogPostsQuery,
  GetBlogPostsQueryVariables,
  GetPagesQuery,
  GetPagesQueryVariables,
} from "../types";

const pagesQueryBody = loader("./getPages.graphql").loc?.source.body;
const blogPostQueryBody = loader("./getBlogPost.graphql").loc?.source.body;
const blogPostContentQueryBody = loader("./getBlogPostContent.graphql").loc
  ?.source.body;

const BaseSDKQuery =
  <T, X>(query?: string) =>
  async (variables: X): Promise<T> =>
    (
      await (
        await fetch(config.CONTENT_BASE_URL, {
          method: "POST",
          headers: {
            "Content-Type": "application/json",
            Accept: "application/json",
          },
          body: JSON.stringify({ query, variables }),
        })
      ).json()
    ).data;

const pagesQuery = BaseSDKQuery<GetPagesQuery, GetPagesQueryVariables>(
  pagesQueryBody
);
const getBlogPostsByCategory = BaseSDKQuery<
  GetBlogPostsQuery,
  GetBlogPostsQueryVariables
>(blogPostQueryBody);

const getBlogPostsContent = BaseSDKQuery<
  GetBlogPostsContentQuery,
  GetBlogPostsContentQueryVariables
>(blogPostContentQueryBody);

const queries = {
  pagesQuery,
  getBlogPostsByCategory,
  getBlogPostsContent
};

export default queries;
