import { AppBar, Container, Link, Toolbar, Typography } from "@mui/material";
import React, { FC } from "react";
import { Link as RouterLink } from "react-router-dom";
import { PageRoute } from "../Router/Router.util";

const Header: FC<{ pages: PageRoute[] }> = ({ pages }) => {
  return (
    <AppBar color="primary" position="static">
      <Container>
        <Toolbar sx={{ flexWrap: "wrap" }}>
          <Typography variant="h6" color="inherit" noWrap sx={{ flexGrow: 1 }}>
            Kroon
          </Typography>
          <nav>
            {pages.map((page) => (
              <Link
                color="inherit"
                underline="hover"
                margin={0.5}
                to={`/${page.url}`}
                key={`link-${page.url}`}
                component={RouterLink}
              >
                {page.title}
              </Link>
            ))}
          </nav>
        </Toolbar>
      </Container>
    </AppBar>
  );
};

export default Header;
