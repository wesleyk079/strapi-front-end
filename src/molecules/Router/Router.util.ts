import { GetPagesQuery, PaginaTemplateDynamicZone } from "../../types";

export type PageRoute = {
  title: string;
  url: string;
  componentProps: PaginaTemplateDynamicZone;
};

export const ResponseToPageRoute = (
  response: GetPagesQuery["paginas"]
): PageRoute[] => {
  const test =
    response?.reduce<PageRoute[]>((acc, item) => {
      // Template should always contain 1 item, this is validated by Strapi back-end
      // Because of Strapi limitation in typing, it will be handled as an array anyways
      // Therefore pick the first item  of the array
      if (item?.Template[0]) {
        const template = item.Template[0];

        if (template.Titel) {
          acc.push({
            title: template.Titel,
            url: template.Titel.replace(/\s+/g, "-").toLowerCase(),
            componentProps: template as PaginaTemplateDynamicZone,
          });
        }
      }

      return acc;
    }, []) || [];
  return test;
};
