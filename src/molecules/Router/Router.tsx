import { Container } from "@mui/material";
import { FC, useCallback, useEffect, useState } from "react";
import { Route, Switch } from "react-router-dom";
import config from "../../config/config";
import Blog from "../../pages/Blog/Blog";
import BlogContent from "../../pages/BlogContent/BlogContent";
import HomePage from "../../pages/Homepage/HomePage";
import queries from "../../queries/queries";
import { GetPagesQuery } from "../../types";
import Header from "../Header/Header";
import { PageRoute, ResponseToPageRoute } from "./Router.util";

const Router: FC = () => {
  const [pages, setPages] = useState<PageRoute[]>([]);

  useEffect(() => {
    const fireQuery = async () => {
      const info = await queries.pagesQuery({});
      setPages(ResponseToPageRoute(info.paginas));
    };
    fireQuery();
  }, []);

  return (
    <>
      <Header pages={pages} />
      <Container sx={{ padding: 2 }}>
        <Switch>
          {pages.map((page) => {
            if (page.componentProps.__typename === "ComponentPageContentBlog") {
              return (
                <>
                  <Route path={`/${page.url}`} exact={true}>
                    <Blog category={page.componentProps.Category} />
                  </Route>
                  <Route path={`/${page.url}/:id`}>
                    <BlogContent />
                  </Route>
                </>
              );
            }
            if (
              page.componentProps.__typename ===
              "ComponentPageContentPaginaContent"
            ) {
              return (
                <Route path={`/${page.url}`}>
                  <HomePage {...page.componentProps} />
                </Route>
              );
            }

            throw new Error("Tried to render page without template");
          })}
        </Switch>
      </Container>
    </>
  );
};

export default Router;
