import { Card, CardMedia, Grid, styled, Typography } from "@mui/material";
import { FC } from "react";

const ImageCard: FC = () => {
  return (
    <Card sx={{ height: "100%", display: "flex", justifyContent: "center" }}>
      <CardMedia
        component="img"
        width="100%"
        src="https://camo.githubusercontent.com/b25f65a5c408dcaeef14b726f979f704a243779bfc87649bf8bb989128306513/68747470733a2f2f7374726170692e696f2f6173736574732f7374726170692d6c6f676f2d6461726b2e737667"
        alt="test"
        sx={{ objectFit: "scale-down" }}
      />
    </Card>
  );
};

export default ImageCard;
