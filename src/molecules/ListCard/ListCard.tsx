import { Star } from "@mui/icons-material";
import {
  Box,
  Card,
  CardMedia,
  Divider,
  Grid,
  List,
  ListItem,
  styled,
  Typography,
} from "@mui/material";
import { yellow } from "@mui/material/colors";
import { FC } from "react";

const StyledList = styled(List)(({ theme }) => ({
  [theme.breakpoints.up("md")]: {
    overflow: "auto",
    maxHeight: "440px",
  },
}));


type ListCardType<T> = {
  items: T[] | undefined;
  render: (content: T) => JSX.Element;
};
const ListCard = <T extends unknown>({ items, render }: ListCardType<T>) => {
  return (
    <Card sx={{ padding: 2, height: "100%" }}>
      <Grid container spacing={1} direction="column" display="flex">
        <Grid item>
          <Typography variant="h6" sx={{ marginBottom: 1 }}>
            Technieken
          </Typography>
        </Grid>
        <Grid item>
          <StyledList>
            {items?.map((listItem, index) => {
              return (
                <Box sx={{ height: "40px" }}>
                  <ListItem>{render(listItem)}</ListItem>
                  {index < items.length - 1 ? <Divider /> : null}
                </Box>
              );
            })}
          </StyledList>
        </Grid>
      </Grid>
    </Card>
  );
};

export default ListCard;
