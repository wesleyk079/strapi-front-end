import { Card, CardMedia, Grid, styled, Typography } from "@mui/material";
import { FC } from "react";

const MediaCard: FC<{ source?: string; alt?: string }> = ({ source, alt }) => {
  return (
    <Card sx={{ padding: 2, height: "100%" }}>
      <Grid container spacing={1}>
        <Grid item width="150px">
          <CardMedia component="img" src={source} alt={alt} />
        </Grid>
        <Grid item xs>
          <Typography variant="h4">Titel</Typography>
          <Typography variant="body2">
            Lorem, ipsum dolor sit amet consectetur adipisicing elit. Accusamus
            adipisci, fugiat explicabo voluptas iste recusandae. Laboriosam
            officia amet praesentium similique quisquam illo, libero porro ab
            laborum ut ratione molestiae fuga.
          </Typography>
        </Grid>
      </Grid>
    </Card>
  );
};

export default MediaCard;
